# mitch 'Framework'

Now I say framework lightly. All this returns is basically an extended express instance, with some opinionated behaviour that I prefer, including:
- loadConfig - Recursively load config files from 'config' directory
- loadMiddleware - Add common middleware into the app. Loads them into the 'session' phase (discussed later)
- loadCustomMiddleware - Recursively loads custom middleware defined in the 'middleware' direectory. Adds them at app._middleware

- loadModules - Loads any mitch-components specified in the modules config. Loads them in the specified order.
- loadUserModules - loads modules specified in the 'modules' directory. Modules will be discussed in future.

### Installation

`npm install mitch`

### Usage

You can find a basic example [here](#). As you can see, it is looks very much like a basic express app, with a few extras.

### Phases
The phase implementation is fairly verbatim from loopback, making use of the loopback-phase module.

### Modules
A module is expected to return a function with the signature `(app) -> {}`, where the returned object can contain a mountPath and server.

At some point during it's boot, it should push models it would like loaded onto the `app._models` array.

### Loading models
All models current must be sequelize models. Each model file should export a function with the signature `(app) => ((sequelize, DataTypes) => model)`, and the module should invoke it with `app` when pushing to `app._models`.

