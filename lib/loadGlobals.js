
var assert = require('assert') ,
    _      = require('lodash') ,
    async  = require('async') ,
    // Todo: Research if moving into a smaller scope will allow
    //   for garbage collection, thus limiting memory usage
    NOOP   = function () {} ,
    debug  = require('debug')('mitch:globals') ;

/**
 * Make properties global if config says so
 */

module.exports = function exposeGlobals (cb) {

  cb = cb || NOOP;

  assert(this.config, "Configuration must exist and be loaded");

  // Globals explicitly disabled
  if ( !this.config.globals ) {
    return;
  }

  debug('Exposing global variables... (you can disable this by modifying the properties in `app.config.globals`)');

  // Provide global access (if allowed in config)
  if (this.config.globals._) {
    global['_'] = _;
  }
  if (this.config.globals.underscoreString) {
    global['_'] = global['_'] || {}
    global['_'].str = require('underscore.string');
    global['_'].mixin(global['_'].str.exports());

    // All functions, include conflict, will be available through _.str object
    global['_'].str.include('Underscore.string', 'string');
  }
  if (this.config.globals.async) {
    global['async'] = async;
  }
  if (this.config.globals.app) {
    global['app'] = this;
  }
  if (this.config.globals.debug) {
    global['debug'] = require('debug')(this.appName);
  }

  // Models and services that should be globalized will be handed in their
  //   appropriate loaders
};
