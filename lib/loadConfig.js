
var requireDir = require('require-directory');
var _ = require('lodash');
var path = require('path');
var NOOP = function () {};

var configDir = 'config';

module.exports = function (cb) {
  cb = cb || NOOP;

  this.config = requireDir(module, path.join(this.baseDir, configDir));

  _.extend(this.config, this.config.index || {});

  if (this.config.modules) {
    _.extend(this.config.modules, this.config.modules.index || {});
    delete this.config.modules.index;
  }
  if (this.config.services) {
    _.extend(this.config.services, this.config.services.index || {});
    delete this.config.services.index;
  }

  cb();

};
