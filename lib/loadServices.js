
var requireDir = require('require-directory');
var path = require('path');
var NOOP = function () {};
var _ = require('lodash');
var debug = require('debug')('mitch:loadServices');

var servicesDir = 'services';

module.exports = function (cb) {
  cb = cb || NOOP;

  var app = this;

  var visitor = function(obj) {
    return obj(app);
  };

  try {
    this.services = requireDir(module, path.join(this.baseDir, servicesDir), { visit: visitor });
  } catch (e) {
    // Typical case of getting here is that the dir doesn't exist
    this.services = {}
  }
  
  // Load npm services
  _.each( app.config.services._ , function (key) {
    var service = require(key);
    app.services[service.name] = service.service;
  });

  // If there is an index file, 'promote' it manually
  _.each( this.services, function (service, key) {
    // debug('Loaded %s service', key);
    if (service.index) {
      app.services[service.index.name] = service.index.service;
    } else {
      app.services[key] = service;
    }
  });

  cb();

};
