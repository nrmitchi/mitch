
var requireDir = require('require-directory');
var path = require('path');
var NOOP = function () {};

var responsesDir = 'responses';

module.exports = function (cb) {
  cb = cb || NOOP;

  try {
    this._responses = requireDir(module, path.join(this.baseDir, responsesDir));
  } catch (e) {
    // Typical case of getting here is that the dir doesn't exist
    this._responses = []
  }

  cb();

};
