
var requireDir = require('require-directory');
var path = require('path');
var NOOP = function () {};

var middlewareDir = 'middleware';

module.exports = function (cb) {
  cb = cb || NOOP;

  try {
    this._middleware = requireDir(module, path.join(this.baseDir, middlewareDir));
  } catch (e) {
    // Typical case of getting here is that the dir doesn't exist
    this._middleware = {};
  }

  cb();

};
