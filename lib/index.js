
var assert = require('assert');
var _ = require('lodash');
var express = require('express');
var merge = require('util')._extend;
var mergePhaseNameLists = require('loopback-phase').mergePhaseNameLists;

var stableSortInPlace = require('stable').inplace;

var BUILTIN_MIDDLEWARE = { builtin: true };

var debug = require('debug')('mitch:index');

var sequelize = require('sequelize');

var proto = {};

module.exports = function mitchExpress(name) {
  var app = express();
  if (name) app.appName = name;
  app.disable('x-powered-by');
  app.baseDir = process.cwd();
  app.__expressLazyRouter = app.lazyrouter;
  app._models = [];
  // Stick my stuff into the express instance
  merge(app, proto);
  merge(app, require('./loadMiddlewarePhasing'));
  return app;
};

// Monkey-patch this in
module.exports.Router = mitchRouter = function mitchRouter(name) {
  var component = express.Router();
  if (name) component.appName = name;
  // Stick my stuff into the Router instance
  // Todo: Add loadModels/loadMiddleware/etc here
  return component;
};

// Allow creating a Router off an app. Creates a reference to the
// app from the Router. This allows methods on the Router to pull
// information from the app (if it exists);
proto.Router = function (name) {
  var router = mitchRouter(name);
  router.app = this;
  return router;
};

proto.loadConfig = require('./loadConfig');
proto.loadGlobals = require('./loadGlobals');
proto.loadMiddleware = require('./loadMiddleware');
proto.loadCustomMiddleware = require('./loadCustomMiddleware');
proto.loadResponses = require('./loadResponses');
proto.loadServices = require('./loadServices');
proto.loadModules = require('./loadModules');
proto.loadUserModules = require('./loadUserModules');
proto.loadModels = require('./loadModels');

proto.initialize = function initialize() {
  /**
   * A convenience function which calls a bunch of loading methods
   */
  this.loadConfig();
  this.loadGlobals();
  this.loadMiddleware();
  this.loadCustomMiddleware();
  this.loadResponses();
  this.loadServices();
};

proto.syncModels = function (fixtures) {
  return _.bind(function (cb) {
    cb = cb || function () {};

    this.sequelize.sync().then( function () {
      if (fixtures) {
        return fixtures(cb);
      }
      return cb();
    }).catch(function(err) {
      return cb(err);
    });
  }, this);
};

// Maybe put these in the constructor, but there shouldn't
// be a situation when they would ever change within a process
proto.isTest = process.env.NODE_ENV == 'test';
proto.isDev = process.env.NODE_ENV == 'development';
proto.isProd = process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'staging';

proto.start = function () {
  var app = this;
  // Bind to local only during production. This forces connections through
  // nginx (or similar), instead of hitting the port directly
  var bindHost = app.isProd || app.is ? 'localhost' : app.config.host;
  
  this.server = this.listen(app.config.port, bindHost, function () {
    debug('Web server listening at: %s:%s', app.config.host, app.config.port);

    if (process.env.NODE_CONSOLE && app.isDev) {
      var REPL = require('repl');

      var repl = REPL.start({
        prompt: '> ',
        input: process.stdin,
        output: process.stdout
      });

      repl.context = _.extend(repl.context, { app: app });
      repl.on('exit', function(err) {
        if (err) {
          process.exit(1);
        }
        process.exit(0);
      });
    }

  });

  process.once('SIGUSR2', _.bind(app.shutdown, app));
  process.on('SIGINT', _.bind(app.shutdown, app)); //_.bind(app.shutdown, app));
  process.on('SIGTERM', _.bind(app.shutdown, app));
};

proto.shutdown = function (e) {
  debug('Shutting down');
  this.server.close();
  process.exit(0);
}
// Add stuff into the Router (loadModels/loadMiddleware)
