
var assert = require('assert');

var requireDir = require('require-directory');
var path = require('path');
var NOOP = function () {};
var _ = require('lodash');
var format = require('string-format');

var Sequelize = require('sequelize');

var debug = require('debug')('mitch:loadModels');

/**
 * Todo: A hash of model overwrites. If an overwrite module is
 *       specified for a particular model, ensure that that modules
 *       Model is the one that ends up in the app.models hash.
 */
module.exports = function (cb) {
  cb = cb || NOOP;

  var app = this;
  var models = this._models;
  this.models = []; // Initialize the actually array that built models can be found in

  // Todo: Move this into an .initialize() function of sorts, that can only be called once
  //       Could probably group loadMiddleware
  var config = app.config.sequelize;

  this.sequelize = new Sequelize(config.database, config.username, config.password, config);

  debug('Creating and linking models');
  _.each( models , function (model) {
    try {
      var Model = model.Factory(app.sequelize, Sequelize);
      app.models[Model.name] = Model;
      debug('  -> %s:%s model loaded', model.module, Model.name);
    } catch (e) {
      debug('  -> Failed to create %s model', model.module);
    }
  });

  Object.keys(this.models).forEach(function(modelName) {
    // Do not try to associate the 'sequelize' or 'Sequelize'
    if (['sequelize', 'Sequelize'].indexOf(modelName) >= 0) return;
    if (app.models[modelName].options.hasOwnProperty('associate')) {
      app.models[modelName].options.associate(app.models);
      if (app.config.globals.models) {
        GLOBAL[modelName] = app.models[modelName];
      }
    }
  });

  cb();

};
