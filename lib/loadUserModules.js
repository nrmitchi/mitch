
var requireDir = require('require-directory');
var path = require('path');
var fs = require('fs');
var NOOP = function () {};
var _ = require('lodash');
var format = require('string-format');

var debug = require('debug')('mitch:loadUserModules');

var moduleDir = 'modules';

module.exports = function (cb) {
  cb = cb || NOOP;

  var app = this;
  var installedModules = this.config.modules;

  try {
    _.each( fs.readdirSync(moduleDir).filter(function (moduleName) {
      if (moduleName == '.DS_Store') {
        debug('\'.DS_Store\' file found. Please take care of this.');
        return false;
      }
      return true;
    }) , function (moduleName) {
      try {
        var module = require(path.join(app.baseDir, moduleDir, moduleName))(app);
        if (module.server) {
          // Todo: make this warp.Logger
          var mountPath = module.mountPath || '/'+moduleName;
          debug('  -> Mounting %s at %s', moduleName, mountPath);

          app.use(mountPath, module.server);
        } else {
          debug('  -> Loading %s', moduleName);
        }
      } catch (e) {
        debug('  -> Could not mount module: %s', moduleName);
        debug(e);
      }
    });
  } catch (e) {
    debug('Could not load user modules. Perhaps the directory is missing?');
    debug(e);
  }

  cb();

};

