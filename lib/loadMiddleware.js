
var _ = require('lodash');
var NOOP = function () {};
// var format = require('string-format');
var debug = require('debug')('mitch:loadMiddleware');

/**
 * This module loads 'basic' middleware into the application,
 * at default phases. In future, I could probably customize the
 * phase that these middlewares get added too.
 */

module.exports = function (cb) {

  // The reason that the key is duplicated in module is incase I have to
  // modify any of the default modules. I can change which module is
  // behind the scene without all configs having to change
  var middlewareModules = {
    'request-ids': { module: 'express-request-id', phase: 'initial:before' } ,
    // 'cookie-parser': { module: 'cookie-parser', phase: 'initial' },  // Todo: Investigate putting this back in for req.cookies
    'method-override': { module: 'method-override', phase: 'initial' },
    'logger': { module: 'morgan', phase: 'initial', options: this.isDev ? 'dev' : 'combined' },
    'response-time': { module: 'response-time', phase: 'initial' }
  };

  // Disable default request logger during tests
  if (this.isTest) {
    delete middlewareModules.logger;
  }

  var app = this;

  cb = cb || NOOP;
  debug('Loading middleware:');
  _.each( middlewareModules , function (options, key) {
    try {
      // Only using default options for now.
      // Todo: Merge app.config.middleware[key] into defaults and
       //      pass to constructor
      var m = require(options.module)(options.options || undefined);
      app.middleware(options.phase, m);
      debug('  -> %s loaded', key);
    } catch (e) {
      debug('  -> Could not load %s', key);
      debug(e);
    }
  });

  /**
    * Load body parser seperately (for now), until I do some fancy delimiting thing
    *
    * If I can access nested attributes with string-dot-notation (bodyParser.json)
    * I could remove this. But I would have to make the above always handle it.
    * For now this works. I may have to fix it when I allow extending the middleware loaded
    * via a config, because I'm sure alot of modules that could be used do something
    * like what body-parser does.
    */

  var cors = require('cors');
  app.middleware('initial', cors(app.config.cors));
  var bodyParser = require('body-parser');
  app.middleware('initial', bodyParser.json());
  app.middleware('initial', bodyParser.urlencoded({ extended: true }));

  /**
   * Another hack, just for now. Should bake this in somewhere nicer
   */
  app.middleware('initial:before', function (req, res, next) {
    res.header('X-Powered-By', 'Mitch');
    next();
  });

  cb();

};
