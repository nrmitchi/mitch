
var requireDir = require('require-directory');
var path = require('path');
var NOOP = function () {};
var _ = require('lodash');
var format = require('string-format');

var debug = require('debug')('mitch:loadModules');

module.exports = function (cb) {
  cb = cb || NOOP;

  var installedModules = this.config.modules._;
  var app = this;

  /**
   * Todo: Consider merging loadModules and loadUserModules
   *       This would only involve needing to add local modules
   *       to the modules config list, but would give more control
   *       over the load order.
   *       Perhaps a config option to autoload out of the modules
   *       directory
   */
  debug(installedModules);
  _.each( installedModules , function (moduleName) {
    try {
      var module = require(moduleName)(app);
      if (module.server) {
        var mountPath = module.mountPath || '/'+moduleName;
        debug('  -> Mounting %s engine at %s', moduleName, mountPath);

        app.use(mountPath, module.server);
      } else {
        debug('  -> Loading %s', moduleName);
      }
    } catch (e) {
      debug('  -> Could not mount module: %s', moduleName);
      debug(e);
    }
  });

  cb();

};
