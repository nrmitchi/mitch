## bin

Commands will go here that can be called with the mitch command line tool

- run
- generate:component
- component:search (search github for mitch components)
- component:install (install component)

### Todo: Figure out how to differentiate between components/services
