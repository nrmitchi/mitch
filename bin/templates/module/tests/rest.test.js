
var should  = require('should') ,
    request = require('supertest') ;

warp.initialize();

// process.env.NODE_ENV = 'test';
var app = warp.app ;  // Todo: This is totally wrong
var api = request(app) ;
var <%= moduleName.singular.capitalized %> = warp.db.<%= moduleName.singular.capitalized %> ;

// console.log(api);

suite('<%= moduleName.plural.capitalized %>', function() {

  suite( 'REST', function() {
    setup(function(){
      
    });

    teardown(function(){

    });

    suite( 'GET /<%= moduleName.plural.lower %>', function() {
      test( 'when logged in', function(done) {
        request(app)
          .get('/<%= moduleName.plural.lower %>')
          .expect('Content-Type', /json/)
          .expect(200)
          .expect(function (res) {
            res.body.<%= moduleName.plural.lower %>.should.be.instanceof(Array)
          })
          .end(done)
      });
      test( 'when not logged in', function(done) {
        request(app)
          .get('/<%= moduleName.plural.lower %>')
          .expect('Content-Type', /json/)
          .expect(401)
          .expect(function (res) {
            // Check that there is no data
            // res.body.<%= moduleName.plural.lower %>.should.be.instanceof(Array)
          })
          .end(done)
      });
    });

    suite( 'GET /<%= moduleName.plural.lower %>/:id', function() {
      test( 'not signed in', function(done) {
        request(app)
          .get('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(401)
          .expect(function (res) {
            res.body.job.should.exist
          })
          .end(done)
      });
      test( 'job user can access', function(done) {
        request(app)
          .get('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(200)
          .expect(function (res) {
            res.body.job.should.exist
          })
          .end(done)
      });
      test( 'job user cannot access', function(done) {
        request(app)
          .get('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(404)
          .expect(function (res) {
            res.body.job.should.exist
          })
          .end(done)
      });
      test( 'job does not exist', function(done) {
        request(app)
          .get('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(404, done)
      });
    });

    suite( 'POST /<%= moduleName.plural.lower %>', function() {
      test( 'with valid input', function(done) {
        request(app)
          .post('/<%= moduleName.plural.lower %>')
          .expect('Content-Type', /json/)
          .expect(201)
          .expect(function (res) {
            res.body.id.should.be.a.Number
          })
          .end(done)
      });
      test( 'invalid input', function(done) {
        request(app)
          .post('/<%= moduleName.plural.lower %>')
          .expect('Content-Type', /json/)
          .expect(400)
          .expect(function (res) {
            res.body.id.should.be.a.Number
          })
          .end(done)
      });
    });

    suite( 'PUT /<%= moduleName.plural.lower %>/:id', function() {
      test( 'a valid update', function(done) {
        request(app)
          .put('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(200)
          .expect(function (res) {
            res.body.job.should.exist
          })
          .end(done)
      });
      test( 'does not have update permission', function(done) {
        request(app)
          .put('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(403)
          .expect(function (res) {
            res.body.job.should.exist
          })
          .end(done)
      });
    });

    suite( 'DELETE /<%= moduleName.plural.lower %>/:id', function() {
      test( 'has delete permission', function(done) {
        request(app)
          .delete('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(204, done)
      });
      test( 'does not have delete permission', function(done) {
        request(app)
          .delete('/<%= moduleName.plural.lower %>/1')
          .expect('Content-Type', /json/)
          .expect(403, done)
      });
    });

  });
});
