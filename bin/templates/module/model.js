
var debug = require('debug')('hooker:<%= moduleName.plural.lower %>:model');

module.exports = function (app) {
  return function(sequelize, DataTypes) {
    var <%= moduleName.singular.capitalized %> = sequelize.define('<%= moduleName.singular.capitalized %>', {
      // ADD MODEL ATTRIBUTES HERE. In future this could be added to generation
      // attribute : {
      //   type : DataTypes.STRING ,
      //   allowNull : false ,
      //   validate : {
      //   }
      // }
    }, {
      paranoid: true ,
      tableName: '<%= moduleName.plural.lower %>' ,
      // comment: '<%= moduleName.singular.capitalized %> database comment' ,
      associate: function(models) {
        // Define relations here
      },
      classMethods: {
        // attributeWhitelist: function(){ return [] }
      },
      instanceMethods: {
        // getFullname: function() {
        //   return [this.first_name, this.last_name].join(' ');
        // }
      }
    });

    <%= moduleName.singular.capitalized %>.beforeValidate( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('beforeValidate - <%= moduleName.singular.capitalized %>');
      return fn(null, <%= moduleName.singular.lower %>);
    });
    <%= moduleName.singular.capitalized %>.afterValidate( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('afterValidate - <%= moduleName.singular.capitalized %>');
      return fn(null, <%= moduleName.singular.lower %>);
    });
    <%= moduleName.singular.capitalized %>.beforeCreate( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('beforeCreate - <%= moduleName.singular.capitalized %>');
      return fn();
    });
    <%= moduleName.singular.capitalized %>.afterCreate( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('afterCreate - <%= moduleName.singular.capitalized %>');
      return fn();
    });
    <%= moduleName.singular.capitalized %>.beforeUpdate( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('beforeUpdate - <%= moduleName.singular.capitalized %>');
      return fn();
    });
    <%= moduleName.singular.capitalized %>.afterUpdate( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('afterUpdate - <%= moduleName.singular.capitalized %>');
      return fn();
    });
    <%= moduleName.singular.capitalized %>.beforeDestroy( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('beforeDestroy - <%= moduleName.singular.capitalized %>');
      return fn();
    });
    <%= moduleName.singular.capitalized %>.afterDestroy( function(<%= moduleName.singular.lower %>, options, fn) {
      debug('afterDestroy - <%= moduleName.singular.capitalized %>');
      return fn();
    });

    return <%= moduleName.singular.capitalized %>;
    
  };
};
