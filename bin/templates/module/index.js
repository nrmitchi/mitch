
module.exports = function (app) {

  // Push models to be loaded
  app._models.push({
    module  : '<%= moduleName.plural.capitalized %>' ,
    Factory : require('./model')(app)
  });

  return {
    mountPath  : '/<%= moduleName.plural.lower %>' ,
    server     : require('./server.js')(app)
  };
};
