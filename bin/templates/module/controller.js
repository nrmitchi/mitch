
module.exports = function (app, modelName) {

  var Blueprints = require('mitch-blueprints')(app, '<%= moduleName.singular.capitalized %>') ;

  return {
    index  : Blueprints.find ,
    show   : Blueprints.findOne ,
    create : Blueprints.create ,
    update : Blueprints.update ,
    delete : Blueprints.destroy
  };
};
