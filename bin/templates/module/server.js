
var _           = require('lodash') ;

module.exports = function (app) {
  var router      = app.Router('<%= moduleName.plural.capitalized %>') ,
      Controller  = require('./controller')(app, '<%= moduleName.singular.capitalized %>') ,
      Blueprints  = require('mitch-blueprints')(app, '<%= moduleName.singular.capitalized %>') ;

  router.param('<%= moduleName.singular.lower %>_id', Blueprints.loadMiddleware );

  var injectUser = app._middleware.injectUser('user_id');
  var authorize = app._middleware.authorize;

  router.use(function (req, res, next) {
    req.options = req.options || {};
    req.options.model = '<%= moduleName.plural.lower %>';
    next();
  });
  
  router.get('/',            [ authorize ], Controller.index );
  router.get('/:<%= moduleName.singular.lower %>_id',     [ authorize ], Controller.show );

  router.post('/',           [ authorize, injectUser ], Controller.create );

  // router.delete('/:job_id', permissionCheck, Controller.delete );

  return router;
};
