## responses

This is where responses should be generated to, and are loaded from. Currently this is not configurable.

This file is mostly here so that git will track the directory.
