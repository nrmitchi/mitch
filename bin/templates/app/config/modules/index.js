
/**
 * The list of modules which should be loaded from NPM modules 
 * 
 * The ones included are default, common modules. To use you will
 * need to `npm install --save <module>`, and uncomment below.
 */

module.exports._ = [
  // 'mitch-component-healthcheck' ,
  // 'mitch-component-authentication' ,
  // 'mitch-component-authorization' ,
  // 'mitch-component-user-subscriptions'
];
