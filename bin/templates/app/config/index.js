
/**
 * These values will be merged directly into the `app.config` object
 */

module.exports = {
  host: process.env.HOST,
  port: process.env.PORT || 2000,
  protocol: 'http'
};
