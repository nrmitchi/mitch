
var debug = require('debug')('sequelize:queries');

module.exports = {
  host     : process.env.POSTGRES_HOST,
  username : process.env.POSTGRES_USER,
  password : process.env.POSTGRES_PASSWORD,
  database : process.env.POSTGRES_DATABASE,
  dialect  : 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  logging: debug
};
