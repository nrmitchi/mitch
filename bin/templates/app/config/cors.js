
/**
 * Cross-Origin Resource Sharing (CORS)
 *
 */

var whitelist = [
  'http://'+process.env.MITCH_BASE_URL, 
  'http://dashboard.'+process.env.MITCH_BASE_URL, 
  'http://www.'+process.env.MITCH_BASE_URL
];

module.exports = {

  // Allow CORS on all routes by default?  If not, you must enable CORS on a
  // per-route basis by either adding a "cors" configuration object
  // to the route config, or setting "cors:true" in the route config to
  // use the default settings below.
  allRoutes: true,

  // Which domains which are allowed CORS access?
  // This can be a comma-delimited list of hosts (beginning with http:// or https://)
  // or "*" to allow all domains CORS access.
  // origins: [
  //   'http://'+process.env.MITCH_BASE_URL, 
  //   'http://dashboard.'+process.env.MITCH_BASE_URL, 
  //   'http://www.'+process.env.MITCH_BASE_URL
  //   ],

  origin: function(origin, callback){
    var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(null, originIsWhitelisted);
  },

  // Allow cookies to be shared for CORS requests?
  credentials: true,

  // Which methods should be allowed for CORS requests?  This is only used
  // in response to preflight requests (see article linked above for more info)
  methods: 'GET, POST, PUT, DELETE, OPTIONS, HEAD',

  // Which headers should be allowed for CORS requests?  This is only used
  // in response to preflight requests.
  headers: 'content-type'

};
