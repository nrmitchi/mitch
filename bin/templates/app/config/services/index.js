
/**
 * The list of services which should be loaded from NPM modules 
 * 
 * The ones included are example services. To use you will
 * need to `npm install --save <service>`, and uncomment below.
 */
module.exports._ = [
  // 'mitch-service-nsq'
];
