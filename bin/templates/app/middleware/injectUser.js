
module.exports = function injectUser (attribute) {
  return function inject (req, res, next) {
    req.body[attribute] = req.user.id;
    next();
  };
};
