## middleware

This is where you can put middleware to be loaded into the `app._middleware` registry.

A basic piece of middleware (included in this directory) is the `injectUser` middleware, which injects the id of the current user into the body of the request. This is very useful for populating a request body to be passed into a Blueprint function.

Middleware included here will be loaded by filename, and nested directories will be loaded as objects. For example:

 - `fakeMiddleware.js` -> `app._middleware.fakeMiddleware`
 - `foo/bar.js` -> `app._middleware.foo.bar`
