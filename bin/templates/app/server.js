
var mitch = require('mitch');
var app = mitch('<%= appName %>');
var _ = require('lodash');

var errorHandler = require('errorhandler');
var expressStatsd = require('express-statsd');
var sequelizeDebug = require('debug')('sequelize:mitch');

app.initialize();

// Feel free to add new stuff here
app.use(expressStatsd({
  client: app.services.stats,
  requestKey: 'statsdRequestKey'
}));

app.use(function (req, res, next) {
  // Calculate and set req.statsdRequestKey
  // <namespace>.<client name>.http.<http verb>.<path>.<segments>
  // This will be taken from auth setting somehow, defaulting to 'unknown'
  var clientName = 'default';
  var requestKey = [
    '<%= appName %>',
    clientName,
    'http',
    req.method.toLowerCase()
  ].concat(_.filter(req.path.split('/'), function (x) { return x.length > 0; }));

  req.statsdRequestKey = requestKey.join('.');

  next();
});

app.loadModules(); // Loads all modules defined in config.modules

// Should only enforce this on user modules
// If these middleware are required for loaded modules, they should be mounted against
// the known mountPath.
app.use([app._middleware.requireUser, app._middleware.checkDeliquency]);

app.loadUserModules(); // Loads all modules defined under ./modules
app.loadModels(); // Actually loads models from app._models into sequelize and builds associations

app.syncModels()(function (err) {
  if (err) {
    console.log (err);
  } else {
    sequelizeDebug('models synced');
  }
});

// Or here

// Perhaps a fancy 404 handler here

// And something to handle errors (just in case)
app.use(errorHandler());

app.start();
