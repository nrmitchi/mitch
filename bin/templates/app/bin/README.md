## bin

This is where you can put scripts to do one off things with your application.

A basic example would be a script to change the password of a user of your application. That script may of may not have been generated into this project (if not you can check the mitch-component-authentication package).

If you need to load your application context (models, connections, etc) into you script, below is a good starting point.

```
// Strict mode just incase
'use strict';

// Todo: Put this in a 'scriptSetup' or something
// Load up the app, although most of it is never used
var mitch = require('mitch');
var app = mitch('hooker-mitch');

app.initialize();

app.loadModules();      // Loads all modules defined in config.modules
app.loadUserModules();  // Loads all modules defined under ./modules
app.loadModels();       // Actually loads models from app._models into sequelize and builds associations

// Pull from args the email and password
var args = process.argv.slice(2);

```

Remember that if you want to use your scripts globally (through an npm install), you will have to point to them from your `package.json` file.
