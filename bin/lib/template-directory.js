
var path = require('path');
var ejs = require('ejs');
var fs = require('fs');
var _ = require('lodash');

function template (src, dest, data) {
  var template = ejs.compile(fs.readFileSync(src, 'utf8'), { cache : src });

  // console.log('Writing to %s', dest);
  fs.writeFileSync(dest, template(data));

}

module.exports = function templateDirectory (src, dest, data) {
  // console.log('Templating %s into %s', src, dest);
  var files = fs.readdirSync(src);

  _.each(files, function (f) {
    var s = fs.statSync(path.join(src, f));
    if (s.isDirectory()) {
      // If this is a directory than we create it, and recurse on its contents
      var new_dir = path.join(dest, f);
      fs.mkdirSync(new_dir);

      templateDirectory(path.join(src, f), new_dir, data);
    } else {
      // Template the file
      template(path.join(src, f), path.join(dest, f), data);
    }
  });
}
